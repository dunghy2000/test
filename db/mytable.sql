-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2020 at 05:34 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `user`
--

-- --------------------------------------------------------

--
-- Table structure for table `mytable`
--

CREATE TABLE `mytable` (
  `Id` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `FullName` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `UserName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Avatar` text COLLATE utf8_unicode_ci NOT NULL,
  `Birth` date NOT NULL,
  `Password` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng 1';

--
-- Dumping data for table `mytable`
--

INSERT INTO `mytable` (`Id`, `FullName`, `UserName`, `Avatar`, `Birth`, `Password`) VALUES
('001', 'Nguyễn Văn A', 'username1', 'https://sites.google.com/site/hinhanhdep24h/_/rsrc/1436687439788/home/hinh%20anh%20thien%20nhien%20dep%202015%20%281%29.jpeg', '1999-01-10', '123456a'),
('002', 'Trần Văn B', 'username2', 'https://sites.google.com/site/hinhanhdep24h/_/rsrc/1436687439788/home/hinh%20anh%20thien%20nhien%20dep%202015%20%281%29.jpeg', '1999-02-14', '123456b'),
('003', 'Nguyễn Thị C', 'username3', '', '1999-03-11', '123456c'),
('004', 'Nguyễn Thị D', 'username4', '', '2001-07-29', '123456d'),
('011', 'Trần Văn E', 'username5', '', '2000-02-09', '123456e'),
('017', 'Hoàng Văn F', 'username6', '', '2002-08-08', '123456f'),
('019', 'Nguyễn Lê G', 'username7', '', '2009-04-25', '123456g'),
('026', 'Nguyễn Trang H', 'username8', '', '2007-07-27', '123456h'),
('055', 'Nguyễn Văn K', 'username9', '', '2000-07-22', '123456k'),
('099', 'Hoàng Lê M', 'username10', '', '1998-02-17', '123456m');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mytable`
--
ALTER TABLE `mytable`
  ADD PRIMARY KEY (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
